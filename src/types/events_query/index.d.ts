export class QueryOptions {
	page?: number;
	size?: number;
	timestamp_from?: number;
	timestamp_to?: number;
}

export class EventsQuery {
	query?: any;
	options?: QueryOptions;
}
