import { Event, UserType, OSType } from './Event';
import Chance from 'chance';

export default generateEvent;

function generateEvent(props: any = {}): Event {
	const chance = new Chance();

	return {
		tenant_id: chance.string(),
		tenant_name: chance.name(),
		id: chance.guid(),
		timestamp: Math.floor(chance.timestamp() * 1000),
		created_date: void 0,

		device_info: {
			id: chance.android_id(),
			os: props.os || chance.pickone([OSType.LINUX, OSType.WINDOWS, OSType.ANDROID, OSType.IOS, OSType.OTHER]),
			os_version: '1'
		},

		user: {
			is_machine: chance.bool(),
			userType: props.userType || chance.pickone([UserType.MEMBER, UserType.ACCOUNT]),
			id: chance.string(),
			name: chance.name(),
			email: chance.email(),
		},

		action: {
			name: chance.pickone(['Open', 'Delete', 'Send', 'Cancel', 'Approve']),
			result: {
				is_success: chance.bool(),
			}
		},

		resource: {
			id: chance.string(),
			name: chance.animal(),
		},
	};
};


