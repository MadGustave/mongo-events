import express from 'express';
import mongodb from 'mongodb';
import {
	InsertWriteOpResult,
	ObjectID,
	MongoClient,
	Db,
	Collection,
	MongoError,
	Cursor
} from 'mongodb';
import { Event } from './Event';
import { EventsQuery, QueryOptions } from 'events_query';

const app = express();

let db;

// async init
(async () => {
	// connect to MongoDB
	let mongoClient: MongoClient;
	let db: Db;

	if(process.env.NODE_ENV === 'test') {
		// connect to test db
		mongoClient = await mongodb.connect((global as any).__MONGO_URI__, { useNewUrlParser: true });
		db = mongoClient.db((global as any).__MONGO_DB_NAME__);
	}
	else {
		mongoClient = await mongodb.connect('mongodb://localhost:27017', { useNewUrlParser: true });
		db = mongoClient.db('events');
	}

	const dbCollection: Collection = db.collection('events');

	// parse incoming JSON
	app.post(['/get', '/add'], express.json());

	// endpoints
	app.post('/get', async (req, res) => {
		const body: EventsQuery = req.body;
		const queryOptions: QueryOptions = body.options;
		const query: any = body.query;
		let dbQuery: any = {};
		let dbQueryOptions: any = {};

		// parse query if there is
		if(typeof query === 'object') {
			const queryKeys: Array<string> = Object.keys(query);

			for(const key of queryKeys) {
				const value = query[key];

				if(value instanceof Array === true) {
					// Array as a value used for logical OR,
					// we need to transform it for mongodb
					dbQuery[key] = { $in: value };
				}
				else {
					// simple key
					dbQuery[key] = value;
				}
			}
		}

		// add timestamp condition
		if(Number.isInteger(queryOptions.timestamp_from) === true) {
			dbQuery.created_date = { $gte: new Date(queryOptions.timestamp_from) };
		}

		if(Number.isInteger(queryOptions.timestamp_to) === true) {
			if(dbQuery.created_date === void 0)
				dbQuery.created_date = { $lte: new Date(queryOptions.timestamp_to) };
			else
				dbQuery.created_date.$lte = new Date(queryOptions.timestamp_to);
		}

		// pagination
		if(Number.isInteger(queryOptions.size) === true && queryOptions.size >= 1) {
			dbQueryOptions.limit = queryOptions.size;

			if(Number.isInteger(queryOptions.page) === true && queryOptions.page >= 1) {
				dbQueryOptions.skip = (queryOptions.page - 1) * queryOptions.size;
			}
		}

		// result sort
		dbQueryOptions.sort = [['created_date', 'descending']];

		const queryCursor: Cursor = dbCollection.find(dbQuery, dbQueryOptions);
		const queryResult: boolean | any[] = await queryCursor.toArray()
			.catch((err: MongoError) => {
				console.error('Some error occured while querying events', err);

				return false;
			});

		if(typeof queryResult === 'boolean') {
			res.status(500).end();
		}
		else {
			// remove DB _id and send response
			res.json(queryResult.map((event) => {
				delete event._id;

				return event;
			}));
		}
	});

	app.post('/add', async (req, res) => {
		const events: any = req.body;

		// should be array of events
		if(events instanceof Array === false)
			return res.status(400).end();

		// DEBUG
		// console.log('add', body);

		// creation timestamp
		const currentTimestamp = new Date();

		// process events
		for(const event of events) {
			// change creation date
			event.created_date = currentTimestamp;
		}

		const insertResult: InsertWriteOpResult | boolean = await dbCollection.insertMany(events)
			.catch((err: MongoError) => {
				console.error('Some error while adding new event to database', err, events);

				return false;
			});

		if(insertResult === false) {
			res.status(500).end();
		}
		else {
			// DEBUG
			// console.log('New events added', events);

			// return addition timestamp
			res.json({
				timestamp: currentTimestamp.getTime(),
			});
		}
	});

	const server: any = app.listen(5000);
	
	if(process.env.NODE_ENV === 'test')
		(app as any)._server = server as any;
})();

export default app;
