export class BaseEvent {
  tenant_id: string; // Tenants are our customers. They might have multiple locations (non tenant events are marked as tenant 0)
  tenant_name: string;
  id: string; // UUID
  timestamp: number; // date when the event actually occurred
  created_date: number; // date when persisted in database - lag can create differences between timestamp -> created date
  transaction_id?: string; // This field will be set when an event is child of a parent event
  is_ce?: boolean; // Marks if the event is a complex event (CE). Complex events have 'children' events. CE event id will be placed in events transaction id
}

export enum OSType {
  LINUX = "Linux",
  WINDOWS = "Windows",
  ANDROID = "Android",
  IOS = "IOS",
  OTHER = "Other"
}

export enum UserType {
  MEMBER = "Member",
  ACCOUNT = "Account"
}

export class DeviceEventData {
  id: string;
  os: OSType;
  os_version: string;
  user_agent?: string;
}

export class ResourceEventData {
  id: string;
  name: string;
}

export class UserEventData {
  is_machine: boolean;
  userType: UserType;
  id: string;
  name: string;
  email?: string; // Optional as machines usually don't have email
}

export class LocationEventData {
  location_id: string;
  location_name: string;
  seating_id?: string;
  seating_name?: string;
}

export class Event extends BaseEvent {
  device_info: DeviceEventData; // Which device the event was sent from
  location_info?: LocationEventData; // Physical events occurs at a specific location. Others are virtual
  user: UserEventData; // Part that describes who has done the action
  action: ActionEventData; // What was done
  resource: ResourceEventData; // EX. Door, Group  - Physical resources MUST contain location details
  additional_info?: object; // Custom / additional information
}

export class ResultEvent {
  is_success: boolean; // Did the requested action succseed
  error_message?: string; // Will be populated on failure. Detailed failure reason
}

export class ActionEventData {
  name: string; // Open, Delete, Send, Cancel, Approve
  result: ResultEvent;
}

/**
 * Examples:
 *  Account has created a new member
 *  Account has associated a member to a team
 *  Member has pressed open door on his mobile
 *  Account has sent mail to everyone
 *  Account has added a new event
 *  Member has purchased an item
 */

/**
 * Insights:
 *  Abnormal rate of failed attempts to do an action in several timeframes (5min, 1h, 12h, 1d)
 *  Abnormal check-in to office (after mid-night + friday + saturday evening)
 *  > 2 access denied events
 *  Abnormal rate of support tickets for a member / team
 *  Get member most loveable items (not sure this is part of events)
 */
