import { MongoClient, Db } from 'mongodb';
import app from '../src/index';
import request from 'supertest';
import generateEvent from '../src/fake_event';
import { Event, OSType, UserType } from '../src/Event';
import { QueryOptions, EventsQuery } from '../src/types/events_query/index.d';

let connection: MongoClient;
let db: Db;
let server: any;

// generate 5 events
const events: Array<Event> = [];

for(let i = 0; i < 5; ++i) {
	events.push(generateEvent());
}

beforeAll(async () => {
	connection = await MongoClient.connect((global as any).__MONGO_URI__, {
		useNewUrlParser: true,
	});
	db = await connection.db((global as any).__MONGO_DB_NAME__);

	// clean db
	await db.collection('events').deleteMany({});

	// add events to db
	await request(app)
		.post('/add')
		.send(events);
});

afterAll(async () => {
	await connection.close();
	await (app as any)._server.close();
});

describe('POST /get', () => {
	it('Should return 2 first events(first page, size is 2)', async () => {
		const queryOpts: QueryOptions = {
			page: 1,
			size: 2,
		};

		const query = {
			options: queryOpts,
		};

		const res = await request(app)
			.post('/get')
			.send(query);

		// delete created_date
		const body = res.body.map((event: Event) => {
			event.created_date = void 0;

			return event;
		});

		expect(body).toEqual(events.slice(0, 2));
	});

	it('Should return just third event(third page, size is 1)', async () => {
		const queryOpts: QueryOptions = {
			page: 3,
			size: 1,
		};

		const query = {
			options: queryOpts,
		};

		const res = await request(app)
			.post('/get')
			.send(query)
			.expect(200);

		// delete created_date
		const body = res.body.map((event: Event) => {
			event.created_date = void 0;

			return event;
		});

		expect(body).toEqual(events.slice(2, 3));
	});

	it('Should return one just added event(timestamp test)', async () => {
		// generate new event
		const newEvent = generateEvent();

		// add new event
		const createdDate = await request(app)
			.post('/add')
			.send([newEvent])
			.expect(200)
			.then((res) => {
				return res.body.timestamp;
			});

		// query that event
		const queryOpts: QueryOptions = {
			page: 1,
			// set size 2 just to make sure there is no second event
			size: 2,
			timestamp_from: createdDate,
		};

		const query = {
			options: queryOpts,
		};

		const fetchedEvents = await request(app)
			.post('/get')
			.send(query)
			.expect(200)
			.then((res) => {
				return res.body;
			});

		expect(fetchedEvents.length).toBe(1);

		// delete created date
		fetchedEvents[0].created_date = void 0;

		expect(fetchedEvents[0]).toEqual(newEvent);
	});

	it('Should return two event by logic OR condition', async () => {
		// collect tenant names(first and third);
		const tenantNames = [events[0].tenant_name, events[2].tenant_name];

		const queryOpts: QueryOptions = {
			page: 1,
			size: 5,
		};

		const query = {
			query: {
				tenant_name: tenantNames,
			},
			options: queryOpts,
		};

		const fetchedEvents = await request(app)
			.post('/get')
			.send(query)
			.expect(200)
			.then((res) => {
				return res.body;
			});

		expect(fetchedEvents.length).toBe(2);

		// delete created_date from fetched
		fetchedEvents[0].created_date = void 0;
		fetchedEvents[1].created_date = void 0;

		expect(fetchedEvents[0]).toEqual(events[0]);
		expect(fetchedEvents[1]).toEqual(events[2]);
	});

	it('Should return events by two keys', async () => {
		// add events with that keys
		const newEvents: Array<Event> = [];

		for(let i = 0; i < 2; ++i) {
			newEvents.push(generateEvent({
				os: OSType.LINUX,
				userType: UserType.ACCOUNT,
			}));
		}

		await request(app)
			.post('/add')
			.send(newEvents)
			.expect(200);

		const queryOpts: QueryOptions = {
			page: 1,
			size: 5,
		};

		const query = {
			query: {
				'device_info.os': OSType.LINUX,
				'user.userType': UserType.ACCOUNT,
			},
			options: queryOpts,
		};

		const fetchedEvents = await request(app)
			.post('/get')
			.send(query)
			.expect(200)
			.then((res) => {
				return res.body;
			});

		for(const event of fetchedEvents) {
			expect(event.device_info.os).toBe(OSType.LINUX);
			expect(event.user.userType).toBe(UserType.ACCOUNT);
		}
	});
});
