import { MongoClient, Db } from 'mongodb';
import app from '../src/index';
import request from 'supertest';
import generateEvent from '../src/fake_event';
import { Event } from '../src/Event';

let connection: MongoClient;
let db: Db;

beforeAll(async () => {
	connection = await MongoClient.connect((global as any).__MONGO_URI__, {
		useNewUrlParser: true,
	});
	db = await connection.db((global as any).__MONGO_DB_NAME__);

	// clean db
	await db.collection('events').deleteMany({});
});

afterAll(async () => {
	await connection.close();
	await (app as any)._server.close();
});

describe('POST /add', () => {
	const events: Array<Event> = [];

	for(let i = 0; i < 3; ++i) {
		events.push(generateEvent());
	}

	it('Should add 3 events', () => {
		return request(app)
			.post('/add')
			.send(events)
			.expect(200);
	});

	it('Should return 3 added events from DB', async () => {
		const res: any = await db.collection('events').find({}, { sort: [['created_date', 'descending']] }).toArray();

		expect(res.length).toBe(events.length);

		for(const [id, event] of res.entries()) {
			// delete ObjectID(Mongo add it on addition)
			delete event._id;

			// also remove created_date
			event.created_date = void 0;

			expect(event).toEqual(events[id]);
		}
	});
});
